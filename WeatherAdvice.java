import java.util.Scanner;
import java.lang.Math;

public class WeatherAdvice {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("������� ���������� � ����������� �������");
        int temperature = scan.nextInt();

        System.out.println("������� ���������� � �������� �����(�/c)");
        int windSpeed = scan.nextInt();

        System.out.println("������� ���������� � ������� �����(���� ���� ������� Y, ����� ������� N");
        char isRaining = scan.next().charAt(0);

        if(isRaining == 'Y'){
            System.out.println("�� ����� �����:����� �������� ����");
        }

        double windTempRatio = 13.12 + 0.6215 * temperature - 11.37 * Math.pow(windSpeed,0.16) + 0.3965 * temperature * Math.pow(windSpeed,0.16);

        if(isRaining == 'N') {
            if (windTempRatio < 0 || windTempRatio > 35) {
                System.out.println("������ ���������������, ��������� ����");
            } else {
                System.out.println("����� ���� ������");
            }
        }
    }

}
